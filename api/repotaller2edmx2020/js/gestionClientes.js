var clientesObtenidos;

function getClientes() {
    //var filter = '?$filter=Country eq \'Germany\'';
    var filter = '';
    var url = 'https://services.odata.org/V4/Northwind/Northwind.svc/Customers' + filter;
    var request = new XMLHttpRequest();

    request.onreadystatechange = function() {
        if(this.readyState == 4 && this.status == 200) {
            //console.table(JSON.parse(request.responseText).value);
            clientesObtenidos = request.responseText;
            procesarClientes();
        }
    }

    request.open("GET", url, true);
    request.send();
}

function procesarClientes() {
    var JSONClientes = JSON.parse(clientesObtenidos);
    //alert(JSONClientes.value[0].ProductName);

    var rutaBandera = 'https://www.countries-ofthe-world.com/flags-normal/flag-of-';
    var divTabla = document.getElementById("divTablaClientes");
    var tabla = document.createElement("table");
    var tbody = document.createElement("tbody");

    tabla.classList.add("table");
    tabla.classList.add("table-striped");

    for (let i = 0; i < JSONClientes.value.length; i++) {
        var nuevafila = document.createElement("tr");

        var columnaNombre = document.createElement("td");
        columnaNombre.innerText = JSONClientes.value[i].ContactName;

        var columnaCity = document.createElement("td");
        columnaCity.innerText = JSONClientes.value[i].City;

        var columnaBandera = document.createElement("td");
        //columnaBandera.innerText = JSONClientes.value[i].Country;
        var imgBandera = document.createElement("img");
        imgBandera.src = rutaBandera + JSONClientes.value[i].Country + '.png';
        imgBandera.classList.add('flag');

        if(JSONClientes.value[i].Country == 'UK') {
            imgBandera.src = rutaBandera + 'United-Kingdom.png';
            columnaBandera.appendChild(imgBandera);
        } else {
            columnaBandera.appendChild(imgBandera);
        }
        
        nuevafila.appendChild(columnaNombre);
        nuevafila.appendChild(columnaCity);
        nuevafila.appendChild(columnaBandera);

        tbody.appendChild(nuevafila);
    }

    tabla.appendChild(tbody);
    divTabla.appendChild(tabla);
}